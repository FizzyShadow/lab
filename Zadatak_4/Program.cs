﻿using System;

namespace Zadatak_4
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount bankAccount = new BankAccount("Bruno Šimuović", "Adresa", 5555);
            Memento memento;
            memento = bankAccount.StoreState();
            Console.WriteLine(bankAccount.ToString());
            bankAccount.UpdateBalance(2000);
            Console.WriteLine(bankAccount.ToString());
            
            Console.WriteLine("Prijašnje stanje: "+memento.Balance);

        }
    }
}
