﻿using System;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            ToDoItem toDoItem = new ToDoItem("Pranje vesa", "Oprati bijelo rublje ", DateTime.Today);
            Memento memento;
            
            CareTaker careTaker = new CareTaker();

            memento = toDoItem.StoreState();
            careTaker.AddMementos(memento);

            Console.WriteLine(toDoItem.ToString());
            toDoItem.Rename("Pranje suđa");
            careTaker.AddMementos(toDoItem.StoreState());
            for(int i = 0; i < careTaker.Count; i++)
            {
                Console.WriteLine(careTaker.GetMemento(i).Title);
            }
        }
    }
}
